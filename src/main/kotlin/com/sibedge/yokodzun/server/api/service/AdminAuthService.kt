package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.common.utils.Validators
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.AdminBaseService
import com.sibedge.yokodzun.server.managers.AdminAuthManager
import com.sibedge.yokodzun.server.managers.ClientAppInstanceManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*


@RestController
class AdminAuthService : AdminBaseService() {


    @PatchMapping("/admin/login")
    fun adminLogin(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("password") password: String,
            @RequestParam("app-instance-uuid") appInstanceUUID: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/admin/login",
            weight = DdosRequestWeight.HARD,
            validateAuthToken = false
    ) {
        ClientAppInstanceManager.validateUUIDOrThrow(appInstanceUUID, logger)
        Validators.validatePasswordOrThrow(password)
        return@handleAdminRequest adminAuthManager.login(password, appInstanceUUID)
    }


    @PatchMapping("/admin/change-password")
    fun adminChangePassword(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("old-password") oldPassword: String,
            @RequestParam("new-password") newPassword: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/admin/change-password",
            weight = DdosRequestWeight.HARD,
            validateAuthToken = false
    ) {
        Validators.validatePasswordOrThrow(oldPassword)
        Validators.validatePasswordOrThrow(newPassword)
        return@handleAdminRequest adminAuthManager.changePassword(oldPassword, newPassword)
    }

}