package com.sibedge.yokodzun.server.api.service.utils

import com.sibedge.yokodzun.server.utils.log.HSessionLog


data class RaterRequestContext(
        val raterCode: String,
        val logger: HSessionLog
) {

    constructor(
            requestContext: RequestContext,
            raterCode: String
    ): this(
            raterCode = raterCode,
            logger = requestContext.logger
    )

}