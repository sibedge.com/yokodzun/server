package com.sibedge.yokodzun.server.api.service.utils

import com.sibedge.yokodzun.server.utils.log.HSessionLog


data class RequestContext(
        val logger: HSessionLog
)