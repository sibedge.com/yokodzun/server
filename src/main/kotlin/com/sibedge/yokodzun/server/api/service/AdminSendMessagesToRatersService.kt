package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.common.data.battle.BattleParameter
import com.sibedge.yokodzun.common.data.battle.Section
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.AdminBaseService
import com.sibedge.yokodzun.server.managers.AdminSendMessagesToRatersManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*


@RestController
class AdminSendMessagesToRatersService : AdminBaseService() {

    @Autowired
    protected lateinit var adminSendMessagesToRatersManager: AdminSendMessagesToRatersManager


    @PutMapping("/battles/{battle-id}/raters-message")
    fun sendMessageToRaters(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String,
            @RequestParam("message") message: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Put:/battles/{battle-id}/raters-message",
            weight = DdosRequestWeight.LIGHT
    ) {
        adminSendMessagesToRatersManager.sendMessageToRaters(
                battleId = battleId,
                sessionLogger = logger,
                message = message
        )
    }

}