package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.common.data.battle.BattleParameter
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.common.utils.Validators
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.AdminBaseService
import com.sibedge.yokodzun.server.managers.AdminAuthManager
import com.sibedge.yokodzun.server.managers.AdminBattlesManager
import com.sibedge.yokodzun.server.managers.AdminYokodzunsManager
import com.sibedge.yokodzun.server.managers.YokodzunsManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*


@RestController
class AdminYokodzunsService : AdminBaseService() {

    @Autowired
    protected lateinit var adminYokodzunsManager: AdminYokodzunsManager

    @PutMapping("/yokodzuns")
    fun yokodzunsNew(
            @RequestHeader headers: HttpHeaders
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Put:/yokodzuns",
            weight = DdosRequestWeight.HARD
    ) {
        adminYokodzunsManager.add()
    }

    @DeleteMapping("/yokodzuns/{yokodzun-id}/remove")
    fun yokodzunRemove(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("yokodzun-id") yokodzunId: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Delete:/yokodzuns/{yokodzun-id}/remove",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminYokodzunsManager.remove(yokodzunId)
    }

    @PatchMapping("/yokodzuns/{yokodzun-id}/description")
    fun yokodzunUpdateDescription(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("yokodzun-id") yokodzunId: String,
            @RequestBody description: Description
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/yokodzuns/{yokodzun-id}/description",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminYokodzunsManager.updateDescription(yokodzunId, description)
    }

}