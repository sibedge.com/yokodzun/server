package com.sibedge.yokodzun.server.api.ddos

import com.google.api.gax.rpc.ApiException


abstract class DdosValidator<K> {
    companion object {
        private const val MINUS_PER_SECOND = 1L
        private const val CRITICAL_SUM = 100L
        private const val MAX_SUM = CRITICAL_SUM * 2
        private const val REQUEST_COUNT_TO_CLEAR = 1000
    }

    private var sumMap: LinkedHashMap<K, Pair<Long, Long>> = LinkedHashMap()

    private var requestsCountForClear = 0

    fun validate(key: K?, requestWeight: DdosRequestWeight) {
        incRequestsCountAndClearIfNeed()
        if (key == null) {
            throwBlockedException(null)
            return
        }

        val lastWeight = sumMap[key]?.let { (time, weight) ->
            calcActualWeight(time, weight)
        } ?: 0L

        val newWeight = lastWeight + requestWeight.value
        sumMap[key] = System.currentTimeMillis() to newWeight

        if (newWeight >= CRITICAL_SUM) {
            throwBlockedException(newWeight - CRITICAL_SUM)
        }

    }

    private fun calcActualWeight(time: Long, weight: Long) =
            (weight - (System.currentTimeMillis() - time) / 1000 * MINUS_PER_SECOND)
                    .coerceIn(0L, MAX_SUM)

    private fun incRequestsCountAndClearIfNeed() = synchronized(this) {
        requestsCountForClear++
        if (requestsCountForClear < REQUEST_COUNT_TO_CLEAR) {
            return@synchronized
        }

        requestsCountForClear = 0

        val newSumMap = LinkedHashMap<K, Pair<Long, Long>>()
        sumMap.forEach { (key, timeWithWeight) ->
            if (calcActualWeight(timeWithWeight.first, timeWithWeight.second) > 0) {
                newSumMap[key] = timeWithWeight
            }

        }
        sumMap = newSumMap
    }

    @Throws(ApiException::class)
    protected abstract fun throwBlockedException(seconds: Long?)

}