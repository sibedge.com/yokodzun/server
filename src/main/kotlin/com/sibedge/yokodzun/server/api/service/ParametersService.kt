package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.CommonBaseService
import com.sibedge.yokodzun.server.managers.ParametersManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class ParametersService : CommonBaseService() {

    @Autowired
    protected lateinit var parametersManager: ParametersManager


    @GetMapping("/parameters")
    fun yokodzunsAll() = handleRequest(
            requestMethodName = "Get:/parameters",
            weight = DdosRequestWeight.LIGHT
    ) {
        parametersManager.getAll()
    }

}