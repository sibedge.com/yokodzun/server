package com.sibedge.yokodzun.server.api.service.base

import com.sibedge.yokodzun.common.api.ApiResponse
import com.sibedge.yokodzun.common.api.ApiUtils
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.utils.RequestContext
import com.sibedge.yokodzun.server.managers.AdminAuthManager
import org.springframework.beans.factory.annotation.Autowired
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.takeIfNotEmpty
import org.springframework.http.HttpHeaders


abstract class AdminBaseService : BaseService() {

    @Autowired
    protected lateinit var adminAuthManager: AdminAuthManager

    fun <T : Any> handleAdminRequest(
            headers: HttpHeaders,
            requestMethodName: String,
            weight: DdosRequestWeight,
            validateAuthToken: Boolean = true,
            block: RequestContext.() -> T
    ) = handleRequest(
            userIdentifier = "Admin",
            weight = weight,
            requestMethodName = requestMethodName
    ) {
        validateAdminRequest(headers, validateAuthToken)
        return@handleRequest block()
    }

    private fun validateAdminRequest(
            headers: HttpHeaders,
            validateAuthToken: Boolean
    ) {
        validateAuthToken.ifFalse { return }
        val authToken = headers[ApiUtils.HEADER_ADMIN_AUTH_TOKEN]?.firstOrNull() ?: ""
        adminAuthManager.validateAuthTokenOrThrow(authToken)
    }


}