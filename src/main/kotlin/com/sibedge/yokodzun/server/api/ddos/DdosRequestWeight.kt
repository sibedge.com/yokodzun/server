package com.sibedge.yokodzun.server.api.ddos


data class DdosRequestWeight(
        val value: Int
) {

    companion object {

        val LIGHT = DdosRequestWeight(1)
        val MEDIUM = DdosRequestWeight(2)
        val HARD = DdosRequestWeight(10)

    }

}