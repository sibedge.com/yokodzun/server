package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.BaseService
import com.sibedge.yokodzun.server.api.service.base.CommonBaseService
import com.sibedge.yokodzun.server.managers.AdminYokodzunsManager
import com.sibedge.yokodzun.server.managers.YokodzunsManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController


@RestController
class YokodzunsService : CommonBaseService() {

    @Autowired
    protected lateinit var yokodzunsManager: YokodzunsManager


    @GetMapping("/yokodzuns")
    fun yokodzunsAll() = handleRequest(
            requestMethodName = "Get:/yokodzuns",
            weight = DdosRequestWeight.LIGHT
    ) {
        yokodzunsManager.getAll()
    }

}