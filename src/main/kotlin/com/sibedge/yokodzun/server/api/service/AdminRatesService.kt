package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.AdminBaseService
import com.sibedge.yokodzun.server.managers.AdminRaterManager
import com.sibedge.yokodzun.server.managers.AdminRatesManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam


@RestController
class AdminRatesService : AdminBaseService() {

    @Autowired
    protected lateinit var adminRatesManager: AdminRatesManager

    @GetMapping("/battles/{battle-id}/rates")
    fun battleRates(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Get:/rates",
            weight = DdosRequestWeight.LIGHT
    ) {
        adminRatesManager.getBattleRates(battleId)
    }


}