package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.common.utils.Validators
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.RaterBaseService
import com.sibedge.yokodzun.server.managers.ClientAppInstanceManager
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*


@RestController
class RaterService : RaterBaseService() {

    @PatchMapping("/rater/login")
    fun raterLogin(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("app-instance-uuid") appInstanceUUID: String
    ) = handleRaterRequest(
            headers = headers,
            requestMethodName = "Patch:/rater/login",
            weight = DdosRequestWeight.HARD
    ) {
        ClientAppInstanceManager.validateUUIDOrThrow(appInstanceUUID, logger)
        raterManager.login(raterCode, appInstanceUUID)
    }

    @GetMapping("/battle/for-rater")
    fun battleForRater(
            @RequestHeader headers: HttpHeaders
    ) = handleRaterRequest(
            headers = headers,
            requestMethodName = "Get:/battle/for-rater",
            weight = DdosRequestWeight.LIGHT
    ) {
        raterManager.getBattle(raterCode)
    }

    @GetMapping("/rater/rates")
    fun raterRates(
            @RequestHeader headers: HttpHeaders
    ) = handleRaterRequest(
            headers = headers,
            requestMethodName = "Get:/rates",
            weight = DdosRequestWeight.LIGHT
    ) {
        raterManager.getRates(raterCode)
    }

    @PutMapping("/battle/{battle-id}/section/{section-id}/parameter/{parameter-id}/yokodzun/{yokodzun-id}/rate")
    fun rate(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String,
            @PathVariable("section-id") sectionId: String,
            @PathVariable("parameter-id") parameterId: String,
            @PathVariable("yokodzun-id") yokodzunId: String,
            @RequestParam("value") value: Float
    ) = handleRaterRequest(
            headers = headers,
            requestMethodName = "Put:/battle/{battle-id}/section/{section-id}/parameter/{parameter-id}/yokodzun/{yokodzun-id}/rate",
            weight = DdosRequestWeight.MEDIUM
    ) {
        raterManager.rate(
                battleId = battleId,
                sectionId = sectionId,
                yokodzunId = yokodzunId,
                parameterId = parameterId,
                raterCode = raterCode,
                value = value
        )
    }


}