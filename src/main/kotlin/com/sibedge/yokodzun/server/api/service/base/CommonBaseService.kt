package com.sibedge.yokodzun.server.api.service.base

import com.sibedge.yokodzun.common.api.ApiUtils
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.utils.RequestContext
import com.sibedge.yokodzun.server.managers.AdminAuthManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import ru.hnau.jutils.ifFalse


abstract class CommonBaseService : BaseService() {

    fun <T : Any> handleRequest(
            requestMethodName: String,
            weight: DdosRequestWeight,
            block: RequestContext.() -> T
    ) = handleRequest(
            weight = weight,
            requestMethodName = requestMethodName,
            userIdentifier = null,
            block = block
    )

}