package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.common.data.battle.BattleParameter
import com.sibedge.yokodzun.common.data.battle.Section
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.common.utils.Validators
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.AdminBaseService
import com.sibedge.yokodzun.server.managers.AdminAuthManager
import com.sibedge.yokodzun.server.managers.AdminBattlesManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*


@RestController
class AdminBattlesService : AdminBaseService() {

    @Autowired
    protected lateinit var adminBattlesManager: AdminBattlesManager


    @GetMapping("/battles")
    fun battlesAll(
            @RequestHeader headers: HttpHeaders
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Get:/battles",
            weight = DdosRequestWeight.LIGHT
    ) {
        adminBattlesManager.getAll()
    }

    @PutMapping("/battles")
    fun battlesNew(
            @RequestHeader headers: HttpHeaders
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Put:/battles",
            weight = DdosRequestWeight.HARD
    ) {
        adminBattlesManager.add()
    }

    @DeleteMapping("/battles/{battle-id}/remove")
    fun battleRemove(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Delete:/battles/{battle-id}/remove",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminBattlesManager.remove(battleId)
    }

    @PatchMapping("/battles/{battle-id}/description")
    fun battleUpdateDescription(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String,
            @RequestBody description: Description
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/battles/{battle-id}/description",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminBattlesManager.updateDescription(battleId, description)
    }

    @PatchMapping("/battles/{battle-id}/sections")
    fun battleUpdateSections(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String,
            @RequestBody sections: List<Section>
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/battles/{battle-id}/sections",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminBattlesManager.updateSections(battleId, sections)
    }

    @PatchMapping("/battles/{battle-id}/parameters")
    fun battleUpdateParameters(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String,
            @RequestBody parameters: List<BattleParameter>
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/battles/{battle-id}/parameters",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminBattlesManager.updateParameters(battleId, parameters)
    }

    @PatchMapping("/battles/{battle-id}/yokodzuns-ids")
    fun battleUpdateYokodzunsIds(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String,
            @RequestBody yokodzunsIds: List<String>
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/battles/{battle-id}/yokodzuns-ids",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminBattlesManager.updateYokodzunsIds(battleId, yokodzunsIds)
    }

    @PatchMapping("/battles/{battle-id}/start")
    fun battleStart(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/battles/{battle-id}/start",
            weight = DdosRequestWeight.LIGHT
    ) {
        adminBattlesManager.startBattle(battleId, logger)
    }

    @PatchMapping("/battles/{battle-id}/stop")
    fun battleStop(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("battle-id") battleId: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/battles/{battle-id}/stop",
            weight = DdosRequestWeight.LIGHT
    ) {
        adminBattlesManager.stopBattle(battleId, logger)
    }

}