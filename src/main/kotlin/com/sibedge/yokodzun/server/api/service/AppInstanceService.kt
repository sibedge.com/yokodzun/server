package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.CommonBaseService
import com.sibedge.yokodzun.server.managers.ClientAppInstanceManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*


@RestController
class AppInstanceService : CommonBaseService() {

    @Autowired
    private lateinit var clientAppInstanceManager: ClientAppInstanceManager

    @PatchMapping("/client-app-instance/{app-instance-uuid}/push-token/{push-token}")
    fun changePushToken(
            @PathVariable("app-instance-uuid") appInstanceUUID: String,
            @PathVariable("push-token") pushToken: String
    ) = handleRequest(
            requestMethodName = "Patch:/client-app-instance/{app-instance-uuid}/push-token/{push-token}",
            weight = DdosRequestWeight.LIGHT
    ) {
        ClientAppInstanceManager.validateUUIDOrThrow(appInstanceUUID, logger)
        ClientAppInstanceManager.validatePushTokenOrThrow(pushToken, logger)

        clientAppInstanceManager.updatePushToken(appInstanceUUID, pushToken)
    }

    @PatchMapping("/on-logout")
    fun onLogout(
            @RequestParam("app-instance-uuid") appInstanceUUID: String
    ) = handleRequest(
            requestMethodName = "Patch:/on-logout",
            weight = DdosRequestWeight.LIGHT
    ) {
        ClientAppInstanceManager.validateUUIDOrThrow(appInstanceUUID, logger)
        clientAppInstanceManager.clearLogin(appInstanceUUID)
    }


}