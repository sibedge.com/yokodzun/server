package com.sibedge.yokodzun.server.api.service.base

import com.sibedge.yokodzun.common.api.ApiResponse
import com.sibedge.yokodzun.common.api.ApiUtils
import com.sibedge.yokodzun.common.exception.ApiException
import com.sibedge.yokodzun.common.utils.RaterCodeUtils
import com.sibedge.yokodzun.common.utils.Validators
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.utils.RaterRequestContext
import com.sibedge.yokodzun.server.api.service.utils.RequestContext
import com.sibedge.yokodzun.server.managers.RaterManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.tryOrElse


abstract class RaterBaseService : BaseService() {

    @Autowired
    protected lateinit var raterManager: RaterManager

    fun <T : Any> handleRaterRequest(
            headers: HttpHeaders,
            requestMethodName: String,
            weight: DdosRequestWeight,
            block: RaterRequestContext.() -> T
    ): ApiResponse<T> {

        val raterCode = headers[ApiUtils.HEADER_RATER_CODE]?.firstOrNull() ?: ""

        return handleRequest(
                userIdentifier = raterCode,
                weight = weight,
                requestMethodName = requestMethodName
        ) {
            (raterCode.length == RaterCodeUtils.LENGTH).ifFalse { throw ApiException.AUTHENTICATION }
            tryOrElse(
                    throwsAction = { raterManager.throwIfRaterNotExists(raterCode) },
                    onThrow = { throw ApiException.AUTHENTICATION }
            )
            val context = RaterRequestContext(this, raterCode)
            return@handleRequest context.block()
        }
    }


}