package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.AdminBaseService
import com.sibedge.yokodzun.server.managers.AdminParametersManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*


@RestController
class AdminParametersService : AdminBaseService() {

    @Autowired
    protected lateinit var adminParametersManager: AdminParametersManager

    @PutMapping("/parameters")
    fun parametersNew(
            @RequestHeader headers: HttpHeaders
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Put:/parameters",
            weight = DdosRequestWeight.HARD
    ) {
        adminParametersManager.add()
    }

    @DeleteMapping("/parameters/{parameter-id}/remove")
    fun parameterRemove(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("parameter-id") parameterId: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Delete:/parameters/{parameter-id}/remove",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminParametersManager.remove(parameterId)
    }

    @PatchMapping("/parameters/{parameter-id}/description")
    fun parameterUpdateDescription(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("parameter-id") parameterId: String,
            @RequestBody description: Description
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Patch:/parameters/{parameter-id}/description",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminParametersManager.updateDescription(parameterId, description)
    }

}