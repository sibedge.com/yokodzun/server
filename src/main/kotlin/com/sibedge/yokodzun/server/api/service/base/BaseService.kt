package com.sibedge.yokodzun.server.api.service.base

import com.sibedge.yokodzun.common.api.ApiResponse
import com.sibedge.yokodzun.common.exception.ApiException
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.ddos.IpDdosValidator
import com.sibedge.yokodzun.server.api.ddos.UserDdosValidator
import com.sibedge.yokodzun.server.api.service.utils.RequestContext
import com.sibedge.yokodzun.server.utils.Utils
import com.sibedge.yokodzun.server.utils.log.HSessionLog
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import ru.hnau.jutils.stringStackTrace


abstract class BaseService {

    fun <T : Any> handleRequest(
            requestMethodName: String,
            userIdentifier: String?,
            weight: DdosRequestWeight,
            block: RequestContext.() -> T
    ): ApiResponse<T> {
        val sessionLog = HSessionLog(
                sessionId = Utils.genBase64UUID(),
                requestMethodName = requestMethodName
        )
        return try {
            val response = handleRequestUnsafe(
                    userIdentifier = userIdentifier,
                    weight = weight,
                    sessionLog = sessionLog,
                    block = block
            )
            sessionLog.d("Finished with response: $response")
            ApiResponse.success(response)
        } catch (ex: Exception) {
            val apiException = if (ex is ApiException) {
                sessionLog.w("Finished with error: $ex")
                ex
            } else {
                sessionLog.e(ex.stringStackTrace)
                ApiException.UNDEFINED
            }
            ApiResponse.error(apiException)
        }
    }

    private fun <T : Any> handleRequestUnsafe(
            userIdentifier: String?,
            weight: DdosRequestWeight,
            sessionLog: HSessionLog,
            block: RequestContext.() -> T
    ): T {

        val ip: String? = (RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes).request.remoteAddr
        sessionLog.d("Request from: $ip")

        IpDdosValidator.validate(ip, weight)

        if (userIdentifier != null) {
            sessionLog.d("User identifier: $userIdentifier")
            UserDdosValidator.validate(userIdentifier, weight)
        }

        val requestContext = RequestContext(sessionLog)
        return requestContext.block()
    }

}