package com.sibedge.yokodzun.server.api.service

import com.sibedge.yokodzun.common.utils.Validators
import com.sibedge.yokodzun.server.api.ddos.DdosRequestWeight
import com.sibedge.yokodzun.server.api.service.base.AdminBaseService
import com.sibedge.yokodzun.server.managers.AdminBattlesManager
import com.sibedge.yokodzun.server.managers.AdminParametersManager
import com.sibedge.yokodzun.server.managers.AdminRaterManager
import com.sibedge.yokodzun.server.managers.EmailManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam


@RestController
class AdminRatersService : AdminBaseService() {

    @Autowired
    protected lateinit var adminRaterManager: AdminRaterManager


    @GetMapping("/raters")
    fun ratersGetForBattle(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("battle-id") battleId: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Get:/raters",
            weight = DdosRequestWeight.LIGHT
    ) {
        adminRaterManager.getBattleRaters(battleId)
    }

    @PutMapping("/raters")
    fun ratersAdd(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("battle-id") battleId: String,
            @RequestParam("count") count: Int
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Put:/raters",
            weight = DdosRequestWeight.HARD
    ) {
        Validators.validateCreateBattlesRatersCount(count)
        adminRaterManager.addRatersForBattle(battleId, count)
    }

    @DeleteMapping("/raters/{rater-code}")
    fun raterRemove(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("rater-code") raterCode: String
    ) = handleAdminRequest(
            headers = headers,
            requestMethodName = "Delete:/raters/{rater-code}",
            weight = DdosRequestWeight.MEDIUM
    ) {
        adminRaterManager.remove(raterCode)
    }


}