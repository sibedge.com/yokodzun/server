package com.sibedge.yokodzun.server.api.ddos

import com.sibedge.yokodzun.common.exception.ApiException


object UserDdosValidator : DdosValidator<String>() {

    override fun throwBlockedException(seconds: Long?) =
            throw ApiException.ddosBlockedUser(seconds)

}