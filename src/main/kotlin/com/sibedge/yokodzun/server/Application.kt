package com.sibedge.yokodzun.server

import com.sibedge.yokodzun.common.data.notification.type.YNotificationCommon
import com.sibedge.yokodzun.common.utils.AuthUtils
import com.sibedge.yokodzun.server.managers.notifications.NotificationsManager
import com.sibedge.yokodzun.server.utils.FirebaseManager
import com.sibedge.yokodzun.server.utils.Utils
import com.sibedge.yokodzun.server.utils.log.HSessionLog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import ru.hnau.jutils.toSingleItemList

@SpringBootApplication
open class Application : CommandLineRunner {

    override fun run(vararg args: String?) {
        FirebaseManager.init()
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(Application::class.java, *args)
        }
    }

}
