package com.sibedge.yokodzun.server.utils

import com.google.firebase.FirebaseApp
import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseOptions
import java.io.FileInputStream


object FirebaseManager {

    fun init() {
        val serviceAccount = FileInputStream("firebase-admin.json")

        val options = FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://yokodzunbattle.firebaseio.com")
                .build()

        FirebaseApp.initializeApp(options)
    }

}