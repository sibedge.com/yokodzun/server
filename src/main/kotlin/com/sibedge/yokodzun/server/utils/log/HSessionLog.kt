package com.sibedge.yokodzun.server.utils.log


class HSessionLog(
        private val sessionId: String,
        private val requestMethodName: String
) : HLogWrapper(
        HBaseLog
) {

    override fun formatMessage(msg: String) = "{$sessionId ($requestMethodName)} $msg"

    fun getMethodLog(className: String, methodName: String) = HMethodLog(this, className, methodName)

    fun getMethodLog(clazz: Class<*>, methodName: String) = HMethodLog(this, clazz, methodName)

}