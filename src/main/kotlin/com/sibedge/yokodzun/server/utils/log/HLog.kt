package com.sibedge.yokodzun.server.utils.log


interface HLog {

    fun i(msg: String, th: Throwable? = null)

    fun t(msg: String, th: Throwable? = null)

    fun d(msg: String, th: Throwable? = null)

    fun w(msg: String, th: Throwable? = null)

    fun e(msg: String, th: Throwable? = null)

}