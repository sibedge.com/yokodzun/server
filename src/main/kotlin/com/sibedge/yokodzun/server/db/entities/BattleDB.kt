package com.sibedge.yokodzun.server.db.entities

import com.sibedge.yokodzun.common.data.Yokodzun
import com.sibedge.yokodzun.common.data.battle.Battle
import com.sibedge.yokodzun.common.data.battle.BattleParameter
import com.sibedge.yokodzun.common.data.battle.BattleStatus
import com.sibedge.yokodzun.common.data.battle.Section
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.server.db.entities.utils.BindingCollection.Companion.KEY_KEY
import com.sibedge.yokodzun.server.db.entities.utils.BindingCollection.Companion.createKey
import com.sibedge.yokodzun.server.db.entities.utils.EntityFabric
import com.sibedge.yokodzun.server.utils.Utils
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document(collection = "battle")
data class BattleDB(
        @Id
        val id: String,
        val created: Long,
        val description: Description,
        val sections: List<Section>,
        val parameters: List<BattleParameter>,
        val yokodzunsIds: List<String>,
        val status: BattleStatus
) {

    companion object : EntityFabric<BattleDB> {

        const val ID_KEY = "id"
        const val CREATED_KEY = "created"
        const val DESCRIPTION_KEY = "description"
        const val SECTIONS_KEY = "sections"
        const val PARAMETERS_KEY = "parameters"
        const val YOKODZUNS_IDS_KEY = "yokodzunsIds"
        const val STATUS_KEY = "status"

        override val entityIDKey = ID_KEY
        override val entityClass = BattleDB::class.java
        override val entityName = "Битва"

        fun new() = BattleDB(
                id = Utils.genUUID(),
                created = System.currentTimeMillis(),
                description = Description(),
                status = BattleStatus.BEFORE,
                parameters = emptyList(),
                sections = emptyList(),
                yokodzunsIds = emptyList()
        )

    }

    val battle
        get() = Battle(
                id = id,
                created = created,
                description = description,
                sections = sections,
                parameters = parameters,
                yokodzunsIds = yokodzunsIds,
                status = status
        )

}