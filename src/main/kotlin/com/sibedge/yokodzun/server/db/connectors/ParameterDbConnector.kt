package com.sibedge.yokodzun.server.db.connectors

import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.server.db.connectors.utils.DbConnector
import com.sibedge.yokodzun.server.db.connectors.utils.query.DbQueryBuilder
import com.sibedge.yokodzun.server.db.connectors.utils.query.inValues
import com.sibedge.yokodzun.server.db.connectors.utils.update.set
import com.sibedge.yokodzun.server.db.entities.ParameterDB
import com.sibedge.yokodzun.server.utils.Utils
import org.springframework.stereotype.Component

@Component
class ParameterDbConnector : DbConnector<ParameterDB, String>(
        entityFabric = ParameterDB
) {

    fun add() = ParameterDB.new().let(this::save).id

    fun remove(id: String) {
        remove(checkID(id))
    }

    fun getAll() =
            findMany(DbQueryBuilder.EMPTY).map(ParameterDB::parameter)

    fun updateDescription(
            id: String,
            description: Description
    ) {
        updateFirst(
                queryBuilder = checkID(id),
                updateBuilder = ParameterDB.DESCRIPTION_KEY set description
        )
    }

    fun throwIfNotExists(id: String) = throwIfNotExists(checkID(id))

}