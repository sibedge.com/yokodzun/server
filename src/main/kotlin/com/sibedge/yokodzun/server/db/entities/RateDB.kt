package com.sibedge.yokodzun.server.db.entities

import com.sibedge.yokodzun.common.data.Rate
import com.sibedge.yokodzun.server.db.entities.utils.BindingCollection
import com.sibedge.yokodzun.server.db.entities.utils.EntityFabric
import org.springframework.data.mongodb.core.mapping.Document


@Document(collection = "rate")
data class RateDB(
        val battleId: String,
        val sectionId: String,
        val yokodzunId: String,
        val parameterId: String,
        val raterCode: String,
        val value: Float
) : BindingCollection(
        key = createKey(battleId, sectionId, yokodzunId, parameterId, raterCode)
) {

    companion object : EntityFabric<RateDB> {

        const val KEY_KEY = BindingCollection.KEY_KEY
        const val BATTLE_ID_KEY = "battleId"
        const val SECTION_ID_KEY = "sectionId"
        const val YOKODZUN_ID_KEY = "yokodzunId"
        const val PARAMETER_ID_KEY = "parameterId"
        const val RATER_CODE_KEY = "raterCode"
        const val VALUE_KEY = "value"

        override val entityIDKey = KEY_KEY
        override val entityClass = RateDB::class.java
        override val entityName = "Оценка"

        fun createKey(
                battleId: String,
                sectionId: String,
                yokodzunId: String,
                parameterId: String,
                raterCode: String
        ) = BindingCollection.createKey(
                battleId,
                sectionId,
                yokodzunId,
                parameterId,
                raterCode
        )

    }

    val rate
        get() = Rate(
                battleId = battleId,
                sectionId = sectionId,
                yokodzunId = yokodzunId,
                parameterId = parameterId,
                raterCode = raterCode,
                value = value
        )

}