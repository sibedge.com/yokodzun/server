package com.sibedge.yokodzun.server.db.connectors

import com.sibedge.yokodzun.common.data.battle.BattleParameter
import com.sibedge.yokodzun.common.data.battle.BattleStatus
import com.sibedge.yokodzun.common.data.battle.Section
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.server.db.connectors.utils.DbConnector
import com.sibedge.yokodzun.server.db.connectors.utils.query.DbQueryBuilder
import com.sibedge.yokodzun.server.db.connectors.utils.query.eq
import com.sibedge.yokodzun.server.db.connectors.utils.query.inValues
import com.sibedge.yokodzun.server.db.connectors.utils.update.set
import com.sibedge.yokodzun.server.db.entities.BattleDB
import com.sibedge.yokodzun.server.db.entities.ParameterDB
import com.sibedge.yokodzun.server.utils.Utils
import org.springframework.stereotype.Component

@Component
class BattleDbConnector : DbConnector<BattleDB, String>(
        entityFabric = BattleDB
) {

    fun add() = BattleDB.new().let(this::save).battle

    fun remove(id: String) {
        remove(checkID(id))
    }

    fun getAll() =
            findMany(DbQueryBuilder.EMPTY).map(BattleDB::battle)

    fun find(ids: List<String>) =
            findMany(BattleDB.ID_KEY inValues ids).map(BattleDB::battle)

    fun get(id: String) =
            findFirstOrThrow(checkID(id)).let(BattleDB::battle)

    fun updateDescription(
            id: String,
            description: Description
    ) {
        updateFirst(
                queryBuilder = checkID(id),
                updateBuilder = BattleDB.DESCRIPTION_KEY set description
        )
    }

    fun updateParameters(
            id: String,
            parameters: List<BattleParameter>
    ) {
        updateFirst(
                queryBuilder = checkID(id),
                updateBuilder = BattleDB.PARAMETERS_KEY set parameters
        )
    }

    fun updateSections(
            id: String,
            sections: List<Section>
    ) {
        updateFirst(
                queryBuilder = checkID(id),
                updateBuilder = BattleDB.SECTIONS_KEY set sections
        )
    }

    fun updateYokodzunsIds(
            id: String,
            yokodzunsIds: List<String>
    ) {
        updateFirst(
                queryBuilder = checkID(id),
                updateBuilder = BattleDB.YOKODZUNS_IDS_KEY set yokodzunsIds
        )
    }

    fun updateStatus(
            id: String,
            newStatus: BattleStatus
    ) {
        updateFirst(
                queryBuilder = checkID(id),
                updateBuilder = BattleDB.STATUS_KEY set newStatus
        )
    }

}