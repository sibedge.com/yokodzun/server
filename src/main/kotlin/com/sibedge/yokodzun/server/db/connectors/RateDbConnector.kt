package com.sibedge.yokodzun.server.db.connectors

import com.sibedge.yokodzun.common.data.battle.BattleParameter
import com.sibedge.yokodzun.common.data.battle.Section
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.server.db.connectors.utils.DbConnector
import com.sibedge.yokodzun.server.db.connectors.utils.query.DbCriterias
import com.sibedge.yokodzun.server.db.connectors.utils.query.DbQueryBuilder
import com.sibedge.yokodzun.server.db.connectors.utils.query.eq
import com.sibedge.yokodzun.server.db.connectors.utils.query.inValues
import com.sibedge.yokodzun.server.db.connectors.utils.update.set
import com.sibedge.yokodzun.server.db.entities.BattleDB
import com.sibedge.yokodzun.server.db.entities.RateDB
import org.springframework.stereotype.Component

@Component
class RateDbConnector : DbConnector<RateDB, String>(
        entityFabric = RateDB
) {

    fun add(
            battleId: String,
            sectionId: String,
            yokodzunId: String,
            parameterId: String,
            raterCode: String,
            value: Float
    ) {
        val rate = RateDB(
                battleId = battleId,
                sectionId = sectionId,
                yokodzunId = yokodzunId,
                parameterId = parameterId,
                raterCode = raterCode,
                value = value
        )
        save(rate)
    }

    fun getBattleRates(battleId: String) =
            findMany(RateDB.BATTLE_ID_KEY eq battleId).map { it.rate }

    fun removeAllOfBattle(battleId: String) {
        remove(RateDB.BATTLE_ID_KEY eq battleId)
    }

    fun removeAllOfYokodzun(yokodzunId: String) {
        remove(RateDB.YOKODZUN_ID_KEY eq yokodzunId)
    }

    fun removeAllOfParameter(parameterId: String) {
        remove(RateDB.PARAMETER_ID_KEY eq parameterId)
    }

    fun checkExistsForBattle(battleId: String) =
            checkIsExists(RateDB.BATTLE_ID_KEY eq battleId)

    fun getRaterRates(raterCode: String) =
            findMany(RateDB.RATER_CODE_KEY eq raterCode).map(RateDB::rate)

}