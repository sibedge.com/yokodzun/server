package com.sibedge.yokodzun.server.db.connectors

import com.sibedge.yokodzun.common.data.battle.BattleParameter
import com.sibedge.yokodzun.common.data.battle.Section
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.common.utils.RANDOM
import com.sibedge.yokodzun.common.utils.RaterCodeUtils
import com.sibedge.yokodzun.server.db.connectors.utils.DbConnector
import com.sibedge.yokodzun.server.db.connectors.utils.query.DbQueryBuilder
import com.sibedge.yokodzun.server.db.connectors.utils.query.eq
import com.sibedge.yokodzun.server.db.connectors.utils.query.inValues
import com.sibedge.yokodzun.server.db.connectors.utils.update.set
import com.sibedge.yokodzun.server.db.entities.BattleDB
import com.sibedge.yokodzun.server.db.entities.BattleRaterDB
import com.sibedge.yokodzun.server.db.entities.ParameterDB
import com.sibedge.yokodzun.server.utils.Utils
import org.springframework.stereotype.Component
import ru.hnau.jutils.ifFalse

@Component
class BattleRaterDbConnector : DbConnector<BattleRaterDB, String>(
        entityFabric = BattleRaterDB
) {

    companion object {

        private val RATER_CODE_AVAILABLE_SYMBOLS = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789".toCharArray()

        private fun generateRaterCode() = String(
                (0 until RaterCodeUtils.LENGTH).map { getRandomCodeSymbol() }.toCharArray()
        )

        private fun getRandomCodeSymbol() =
                RATER_CODE_AVAILABLE_SYMBOLS[RANDOM.nextInt(RATER_CODE_AVAILABLE_SYMBOLS.size)]

    }

    fun addForBattle(battleId: String): String {
        var code = generateUniqueRaterCode()
        val battleRater = BattleRaterDB(
                raterCode = code,
                battleId = battleId
        )
        save(battleRater)
        return code
    }

    private fun generateUniqueRaterCode(): String {
        while (true) {
            val code = generateRaterCode()
            checkIsExists(checkID(code)).ifFalse { return code }
        }
    }

    fun remove(id: String) {
        remove(checkID(id))
    }

    fun removeAllOfBattle(battleId: String) =
            remove(BattleRaterDB.BATTLE_ID_KEY eq battleId)

    fun getBattleRaters(
            battleId: String
    ) = findMany(
            BattleRaterDB.BATTLE_ID_KEY eq battleId
    ).map { it.raterCode }

    fun throwIfRaterIsNotForBattle(
            raterCode: String,
            battleId: String
    ) = throwIfNotExists(
            (BattleRaterDB.RATER_CODE_KEY eq raterCode) +
                    (BattleRaterDB.BATTLE_ID_KEY eq battleId)
    )

    fun throwIfRaterNotExists(raterCode: String) =
            throwIfNotExists(checkID(raterCode))

    fun getRaterBattleId(raterCode: String) =
            findFirstOrThrow(checkID(raterCode)).battleId

}