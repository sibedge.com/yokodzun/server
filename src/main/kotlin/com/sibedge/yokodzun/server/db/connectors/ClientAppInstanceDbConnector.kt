package com.sibedge.yokodzun.server.db.connectors

import com.sibedge.yokodzun.server.db.connectors.utils.DbConnector
import com.sibedge.yokodzun.server.db.connectors.utils.query.eq
import com.sibedge.yokodzun.server.db.connectors.utils.query.ne
import com.sibedge.yokodzun.server.db.connectors.utils.update.set
import com.sibedge.yokodzun.server.db.entities.ClientAppInstanceDB
import org.springframework.stereotype.Component


@Component
class ClientAppInstanceDbConnector : DbConnector<ClientAppInstanceDB, String>(
        entityFabric = ClientAppInstanceDB
) {

    fun updateLogin(
            clientAppInstanceUUID: String,
            login: String
    ) = createOrUpdate(
            clientAppInstanceUUID = clientAppInstanceUUID,
            fieldKey = ClientAppInstanceDB.USER_LOGIN_KEY,
            fieldValue = login,
            loginForNewInstance = login,
            pushTokenForNewInstance = "",
            isNeedUpdateChecker = { it.userLogin != login }
    )

    fun updatePushToken(
            clientAppInstanceUUID: String,
            pushToken: String
    ) = createOrUpdate(
            clientAppInstanceUUID = clientAppInstanceUUID,
            fieldKey = ClientAppInstanceDB.PUSH_TOKEN_KEY,
            fieldValue = pushToken,
            loginForNewInstance = "",
            pushTokenForNewInstance = pushToken,
            isNeedUpdateChecker = { it.pushToken != pushToken }
    )

    fun getPushTokensForLogin(login: String) =
            findMany(
                    (ClientAppInstanceDB.USER_LOGIN_KEY eq login) +
                            (ClientAppInstanceDB.PUSH_TOKEN_KEY ne "")
            ).mapNotNull { it.pushToken }

    private fun createOrUpdate(
            clientAppInstanceUUID: String,
            fieldKey: String,
            fieldValue: Any,
            loginForNewInstance: String,
            pushTokenForNewInstance: String,
            isNeedUpdateChecker: (ClientAppInstanceDB) -> Boolean
    ) {
        val existence =
                findFirstOrNull(checkID(clientAppInstanceUUID))

        if (existence != null) {
            if (!isNeedUpdateChecker.invoke(existence)) {
                return
            }
            updateFirst(checkID(clientAppInstanceUUID), fieldKey set fieldValue)
            return
        }

        val new = ClientAppInstanceDB(
                uuid = clientAppInstanceUUID,
                pushToken = pushTokenForNewInstance,
                userLogin = loginForNewInstance
        )
        save(new)
    }

}