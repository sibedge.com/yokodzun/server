package com.sibedge.yokodzun.server.db.connectors

import com.sibedge.yokodzun.server.db.connectors.utils.DbConnector
import com.sibedge.yokodzun.server.db.connectors.utils.query.eq
import com.sibedge.yokodzun.server.db.entities.SettingDB
import com.sibedge.yokodzun.server.managers.settings.value.utils.Settings
import org.springframework.stereotype.Component


@Component
class SettingDbConnector : DbConnector<SettingDB, String>(
        entityFabric = SettingDB
), Settings {

    override fun set(key: String, value: String) {
        save(SettingDB(key, value))
    }

    override fun get(key: String) =
            findFirstOrNull(SettingDB.KEY_KEY eq key)?.value

}