package com.sibedge.yokodzun.server.db.entities.utils

import com.sibedge.yokodzun.server.utils.Utils
import org.springframework.data.annotation.Id


abstract class BindingCollection(
        @Id var key: String?
) {

    companion object {

        const val KEY_KEY = "key"

        fun createKey(vararg keyParts: String?) =
                Utils.getStringsKey(*keyParts)

    }

}