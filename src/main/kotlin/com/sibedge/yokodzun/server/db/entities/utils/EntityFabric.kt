package com.sibedge.yokodzun.server.db.entities.utils


interface EntityFabric<T : Any> {

    val entityIDKey: String
    val entityClass: Class<T>
    val entityName: String

}