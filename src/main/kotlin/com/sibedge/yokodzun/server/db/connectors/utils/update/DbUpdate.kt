package com.sibedge.yokodzun.server.db.connectors.utils.update


data class DbUpdate(
        val key: String,
        val value: Any
)

