package com.sibedge.yokodzun.server.db.connectors.utils.query

import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.inValues
import org.springframework.data.mongodb.core.query.isEqualTo


class DbCriterias(
        private val criterias: Iterable<Criteria>
) : DbQueryBuilder {

    constructor(
            criteria: Criteria
    ) : this(
            listOf(criteria)
    )

    override fun buildQuery() =
            Query().apply { criterias.forEach { addCriteria(it) } }

    operator fun plus(criteria: Criteria) =
            DbCriterias(criterias + criteria)

    operator fun plus(other: DbCriterias) =
            DbCriterias(criterias + other.criterias)

}

infix fun String.lt(value: Any) =
        DbCriterias(Criteria.where(this).lt(value))

infix fun String.lte(value: Any) =
        DbCriterias(Criteria.where(this).lte(value))

infix fun String.gt(value: Any) =
        DbCriterias(Criteria.where(this).gt(value))

infix fun String.gte(value: Any) =
        DbCriterias(Criteria.where(this).gte(value))

infix fun String.eq(value: Any) =
        DbCriterias(Criteria.where(this).isEqualTo(value))

infix fun String.inValues(value: Any) =
        DbCriterias(Criteria.where(this).`in`(value))

infix fun String.ne(value: Any) =
        DbCriterias(Criteria.where(this).ne(value))