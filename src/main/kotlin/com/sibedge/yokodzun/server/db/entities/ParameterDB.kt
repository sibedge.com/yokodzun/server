package com.sibedge.yokodzun.server.db.entities

import com.sibedge.yokodzun.common.data.Parameter
import com.sibedge.yokodzun.common.data.Yokodzun
import com.sibedge.yokodzun.common.data.battle.Battle
import com.sibedge.yokodzun.common.data.battle.BattleParameter
import com.sibedge.yokodzun.common.data.battle.Section
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.server.db.entities.utils.BindingCollection.Companion.KEY_KEY
import com.sibedge.yokodzun.server.db.entities.utils.BindingCollection.Companion.createKey
import com.sibedge.yokodzun.server.db.entities.utils.EntityFabric
import com.sibedge.yokodzun.server.utils.Utils
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document(collection = "parameter")
data class ParameterDB(
        @Id
        val id: String,
        val description: Description
) {

    companion object : EntityFabric<ParameterDB> {

        const val ID_KEY = "id"
        const val DESCRIPTION_KEY = "description"

        override val entityIDKey = ID_KEY
        override val entityClass = ParameterDB::class.java
        override val entityName = "Параметр"

        fun new() = ParameterDB(
                id = Utils.genUUID(),
                description = Description()
        )

    }

    val parameter
        get() = Parameter(
                id = id,
                description = description
        )

}