package com.sibedge.yokodzun.server.db.entities

import com.sibedge.yokodzun.server.db.entities.utils.BindingCollection
import com.sibedge.yokodzun.server.db.entities.utils.EntityFabric
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document(collection = "battle-rater")
data class BattleRaterDB(
        @Id
        val raterCode: String,
        val battleId: String
) {

    companion object : EntityFabric<BattleRaterDB> {

        const val RATER_CODE_KEY = "raterCode"
        const val BATTLE_ID_KEY = "battleId"

        override val entityIDKey = RATER_CODE_KEY
        override val entityClass = BattleRaterDB::class.java
        override val entityName = "Зритель оценивающий битву"

    }
}