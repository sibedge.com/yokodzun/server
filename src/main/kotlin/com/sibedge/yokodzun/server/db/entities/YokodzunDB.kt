package com.sibedge.yokodzun.server.db.entities

import com.sibedge.yokodzun.common.data.Yokodzun
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.server.db.entities.utils.BindingCollection.Companion.KEY_KEY
import com.sibedge.yokodzun.server.db.entities.utils.BindingCollection.Companion.createKey
import com.sibedge.yokodzun.server.db.entities.utils.EntityFabric
import com.sibedge.yokodzun.server.utils.Utils
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document(collection = "yokodzun")
data class YokodzunDB(
        @Id
        val id: String,
        val description: Description
) {

    companion object : EntityFabric<YokodzunDB> {

        const val ID_KEY = "id"
        const val DESCRIPTION_KEY = "description"

        override val entityIDKey = ID_KEY
        override val entityClass = YokodzunDB::class.java
        override val entityName = "Ёкодзуна"

        fun new() = YokodzunDB(
                id = Utils.genUUID(),
                description = Description()
        )

    }

    val yokodzun
        get() = Yokodzun(
                id = id,
                description = description
        )

}