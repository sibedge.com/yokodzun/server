package com.sibedge.yokodzun.server.db.connectors.utils.query

import org.springframework.data.mongodb.core.query.Query


interface DbQueryBuilder {

    companion object {

        val EMPTY: DbQueryBuilder = DbRawQueryBuilder.EMPTY

    }

    fun buildQuery(): Query

    fun toRaw() =
            DbRawQueryBuilder(buildQuery())

}
