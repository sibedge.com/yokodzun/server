package com.sibedge.yokodzun.server.db.entities

import com.sibedge.yokodzun.server.db.entities.utils.EntityFabric
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document(collection = "settings")
data class SettingDB(
        @Id
        val key: String,
        val value: String
) {

    companion object : EntityFabric<SettingDB> {

        const val KEY_KEY = "key"
        const val VALUE_KEY = "value"

        override val entityIDKey = KEY_KEY
        override val entityClass = SettingDB::class.java
        override val entityName = "Настройка"

    }

}