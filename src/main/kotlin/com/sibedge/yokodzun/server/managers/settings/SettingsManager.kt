package com.sibedge.yokodzun.server.managers.settings

import com.sibedge.yokodzun.server.db.connectors.SettingDbConnector
import com.sibedge.yokodzun.server.managers.settings.value.StringSetting
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class SettingsManager {

    @Autowired
    private lateinit var settingsDb: SettingDbConnector

    private val settingsProvider = { settingsDb }

    var adminPasswordHash by StringSetting("adminPasswordHash", settingsProvider)
    var adminAuthToken by StringSetting("adminAuthToken", settingsProvider)

}