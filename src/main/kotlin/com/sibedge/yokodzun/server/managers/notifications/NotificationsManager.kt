package com.sibedge.yokodzun.server.managers.notifications

import com.google.firebase.messaging.FirebaseMessaging
import com.sibedge.yokodzun.common.data.notification.YNotification
import com.sibedge.yokodzun.common.data.notification.YToUserNotification
import com.sibedge.yokodzun.server.managers.ClientAppInstanceManager
import com.sibedge.yokodzun.server.utils.log.HMethodLog
import com.sibedge.yokodzun.server.utils.log.HSessionLog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.takeIfNotEmpty
import ru.hnau.jutils.tryCatch
import java.util.concurrent.Executors
import java.util.concurrent.Future

@Component
class NotificationsManager {

    @Autowired
    private lateinit var clientAppInstanceManager: ClientAppInstanceManager

    private val firebaseMessaging: FirebaseMessaging by lazy {
        FirebaseMessaging.getInstance()
    }

    private val sendNotificationCallbackExecutor =
            Executors.newSingleThreadExecutor()


    fun send(
            logins: Iterable<String>,
            notification: YNotification,
            sessionLogger: HSessionLog
    ) {

        val serializedNotification =
                notification.serialize()

        val logger = sessionLogger.getMethodLog(NotificationsManager::class.java, "send")

        logins.forEach { toUserLogin ->

            val pushTokens = clientAppInstanceManager
                    .getPushTokensForLogin(toUserLogin)
                    .takeIfNotEmpty()
                    ?: return@forEach

            val toUserNotification = YToUserNotification(
                    toUser = toUserLogin,
                    notification = serializedNotification
            )

            pushTokens.forEach { pushToken ->
                sendToUserAsync(
                        toUserNotification = toUserNotification,
                        pushToken = pushToken,
                        logger = logger
                )
            }

        }
    }

    private fun sendToUserAsync(
            toUserNotification: YToUserNotification,
            pushToken: String,
            logger: HMethodLog
    ) {
        val message = toUserNotification.createMessage(pushToken)
        firebaseMessaging.sendAsync(message).let { future ->
            future.addListener(
                    Runnable {
                        handleSendResult(
                                toUserNotification,
                                future,
                                logger
                        )
                    },
                    sendNotificationCallbackExecutor
            )
        }
    }

    private fun handleSendResult(
            toUserNotification: YToUserNotification,
            future: Future<String>,
            logger: HMethodLog
    ) {
        val login = toUserNotification.toUser
        tryCatch(
                throwsAction = {
                    val messageID = future.get()
                    logger.d("Sent to user '$login' message '$messageID'")
                },
                onThrow = { th ->
                    logger.w("Unable send message to user '$login': ${th.message}")
                }
        )
    }

}