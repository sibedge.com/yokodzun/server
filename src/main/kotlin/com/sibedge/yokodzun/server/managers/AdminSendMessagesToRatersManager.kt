package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.common.data.notification.type.YNotificationCommon
import com.sibedge.yokodzun.server.db.connectors.BattleRaterDbConnector
import com.sibedge.yokodzun.server.db.connectors.RateDbConnector
import com.sibedge.yokodzun.server.managers.notifications.NotificationsManager
import com.sibedge.yokodzun.server.utils.log.HSessionLog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.me


@Component
class AdminSendMessagesToRatersManager {

    @Autowired
    private lateinit var battleRaterDb: BattleRaterDbConnector

    @Autowired
    private lateinit var notificationsManager: NotificationsManager

    fun sendMessageToRaters(
            battleId: String,
            message: String,
            sessionLogger: HSessionLog
    ) {
        val ratersCodes = battleRaterDb.getBattleRaters(battleId)
        notificationsManager.send(
                logins = ratersCodes,
                notification = YNotificationCommon(message),
                sessionLogger = sessionLogger
        )
    }

}