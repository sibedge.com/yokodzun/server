package com.sibedge.yokodzun.server.managers

import org.springframework.stereotype.Component
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.mail.SimpleMailMessage


@Component
class EmailManager {

    @Autowired
    private lateinit var emailSender: JavaMailSender

    @Autowired
    protected lateinit var environment: Environment

    private val address
            by lazy { environment.getProperty("com.sibedge.yokodzun.mail.address")!! }

    fun send(
            subject: String,
            text: String
    ) = emailSender.send(
            SimpleMailMessage().apply {
                setTo(address)
                setSubject(subject)
                setText(text)
            }
    )

}