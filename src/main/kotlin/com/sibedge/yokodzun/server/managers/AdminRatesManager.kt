package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.server.db.connectors.BattleRaterDbConnector
import com.sibedge.yokodzun.server.db.connectors.RateDbConnector
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class AdminRatesManager {

    @Autowired
    private lateinit var rateDb: RateDbConnector

    fun getBattleRates(battleId: String) =
            rateDb.getBattleRates(battleId)

}