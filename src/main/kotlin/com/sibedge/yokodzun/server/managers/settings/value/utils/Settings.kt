package com.sibedge.yokodzun.server.managers.settings.value.utils

import com.sibedge.yokodzun.server.db.connectors.utils.query.eq
import com.sibedge.yokodzun.server.db.entities.SettingDB


interface Settings {

    fun set(key: String, value: String)

    fun get(key: String): String?

}