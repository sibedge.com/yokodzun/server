package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.common.data.battle.BattleParameter
import com.sibedge.yokodzun.common.data.battle.BattleStatus
import com.sibedge.yokodzun.common.data.battle.Section
import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.common.data.notification.type.YNotificationBattleStarted
import com.sibedge.yokodzun.common.data.notification.type.YNotificationBattleStopped
import com.sibedge.yokodzun.common.exception.ApiException
import com.sibedge.yokodzun.server.db.connectors.BattleDbConnector
import com.sibedge.yokodzun.server.db.connectors.BattleRaterDbConnector
import com.sibedge.yokodzun.server.db.connectors.RateDbConnector
import com.sibedge.yokodzun.server.managers.notifications.NotificationsManager
import com.sibedge.yokodzun.server.utils.log.HSessionLog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.ifTrue


@Component
class AdminBattlesManager {

    @Autowired
    private lateinit var battleDb: BattleDbConnector

    @Autowired
    private lateinit var battleRaterDb: BattleRaterDbConnector

    @Autowired
    private lateinit var rateDb: RateDbConnector

    @Autowired
    private lateinit var notificationsManager: NotificationsManager

    fun add() = battleDb.add()

    fun getAll() = battleDb.getAll()

    fun getById(id: String) = battleDb.get(id)

    fun remove(id: String) {
        rateDb.removeAllOfBattle(id)
        battleRaterDb.removeAllOfBattle(id)
        battleDb.remove(id)
    }

    fun updateDescription(
            id: String,
            description: Description
    ) = battleDb.updateDescription(
            id = id,
            description = description
    )

    fun updateParameters(
            id: String,
            parameters: List<BattleParameter>
    ) {
        throwIfBattleAlreadyRated(id)
        battleDb.updateParameters(
                id = id,
                parameters = parameters
        )
    }

    fun updateSections(
            id: String,
            sections: List<Section>
    ) {
        throwIfBattleAlreadyRated(id)
        battleDb.updateSections(
                id = id,
                sections = sections
        )
    }

    fun updateYokodzunsIds(
            id: String,
            yokodzunsIds: List<String>
    ) {
        throwIfBattleAlreadyRated(id)
        battleDb.updateYokodzunsIds(
                id = id,
                yokodzunsIds = yokodzunsIds
        )
    }

    fun startBattle(
            id: String,
            sessionLogger: HSessionLog
    ) {
        val battle = battleDb.get(id)
        if (battle.parameters.isEmpty()) {
            throw ApiException.raw("Для битвы ${battle.description.title} не заданы параметры")
        }
        if (battle.yokodzunsIds.isEmpty()) {
            throw ApiException.raw("Для битвы ${battle.description.title} не заданы ёкодзуны")
        }
        if (battle.sections.isEmpty()) {
            throw ApiException.raw("Для битвы ${battle.description.title} не заданы разделы")
        }
        battleDb.updateStatus(id, BattleStatus.IN_PROGRESS)

        val battleRatersCodes = battleRaterDb.getBattleRaters(battleId = id)
        notificationsManager.send(
                logins = battleRatersCodes,
                notification = YNotificationBattleStarted(
                        battleId = battle.id,
                        battleName = battle.description.title
                ),
                sessionLogger = sessionLogger
        )
    }

    fun stopBattle(
            id: String,
            sessionLogger: HSessionLog
    ) {
        battleDb.updateStatus(id, BattleStatus.AFTER)

        val battle = battleDb.get(id)

        val battleRatersCodes = battleRaterDb.getBattleRaters(battleId = id)
        notificationsManager.send(
                logins = battleRatersCodes,
                notification = YNotificationBattleStopped(
                        battleId = battle.id,
                        battleName = battle.description.title
                ),
                sessionLogger = sessionLogger
        )
    }

    private fun throwIfBattleAlreadyRated(id: String) {
        rateDb.checkExistsForBattle(id).ifTrue { throw ApiException.raw("Данная битва уже оценена") }
    }

}