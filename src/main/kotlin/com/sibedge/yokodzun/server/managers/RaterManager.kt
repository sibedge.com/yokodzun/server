package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.common.data.battle.Battle
import com.sibedge.yokodzun.common.data.battle.BattleStatus
import com.sibedge.yokodzun.common.exception.ApiException
import com.sibedge.yokodzun.server.db.connectors.BattleDbConnector
import com.sibedge.yokodzun.server.db.connectors.BattleRaterDbConnector
import com.sibedge.yokodzun.server.db.connectors.ParameterDbConnector
import com.sibedge.yokodzun.server.db.connectors.RateDbConnector
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.ifFalse


@Component
class RaterManager {

    @Autowired
    private lateinit var rateDb: RateDbConnector

    @Autowired
    private lateinit var battleRaterDb: BattleRaterDbConnector

    @Autowired
    private lateinit var battleDb: BattleDbConnector

    @Autowired
    private lateinit var clientAppInstanceManager: ClientAppInstanceManager

    fun throwIfRaterNotExists(raterCode: String) =
            battleRaterDb.throwIfRaterNotExists(raterCode)

    fun login(
            raterCode: String,
            appInstancewUUID: String
    ) {

        clientAppInstanceManager.updateLogin(appInstancewUUID, raterCode)
    }

    fun getBattle(raterCode: String): Battle {
        val battleId = battleRaterDb.getRaterBattleId(raterCode)
        return battleDb.get(battleId)
    }

    fun getRates(raterCode: String) =
            rateDb.getRaterRates(raterCode)

    fun rate(
            battleId: String,
            sectionId: String,
            yokodzunId: String,
            parameterId: String,
            raterCode: String,
            value: Float
    ) {
        if (value > 1 || value < 0) {
            throw ApiException.raw("Значение выходит за пределы диапазона")
        }
        throwIfRaterIsNotForBattle(
                raterCode = raterCode,
                battleId = battleId
        )
        val battle = battleDb.get(battleId)
        battle.yokodzunsIds.any { it == yokodzunId }.ifFalse {
            throw ApiException.raw("Ёкодзуна не учавствует в битве")
        }
        battle.parameters.any { it.parameterId == parameterId }.ifFalse {
            throw ApiException.raw("Параметр не используется в битве")
        }
        battle.sections.any { it.id == sectionId }.ifFalse {
            throw ApiException.raw("Секция отсутствует в битве")
        }
        if (battle.status == BattleStatus.BEFORE) {
            throw ApiException.raw("Битва ещё не началась")
        }
        if (battle.status == BattleStatus.AFTER) {
            throw ApiException.raw("Битва уже закончилась")
        }
        rateDb.add(
                battleId = battleId,
                sectionId = sectionId,
                yokodzunId = yokodzunId,
                parameterId = parameterId,
                raterCode = raterCode,
                value = value
        )
    }

    private fun throwIfRaterIsNotForBattle(
            raterCode: String,
            battleId: String
    ) = battleRaterDb.throwIfRaterIsNotForBattle(
            raterCode = raterCode,
            battleId = battleId
    )

}