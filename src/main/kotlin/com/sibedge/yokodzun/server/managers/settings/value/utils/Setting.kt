package com.sibedge.yokodzun.server.managers.settings.value.utils

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


abstract class Setting<T>(
        private val key: String,
        private val settingsProvider: () -> Settings,
        private val defaultValue: T
) : ReadWriteProperty<Any?, T> {

    override fun getValue(
            thisRef: Any?,
            property: KProperty<*>
    ) = settingsProvider()
            .get(key)
            ?.let(this::stringToValue)
            ?: defaultValue

    override fun setValue(
            thisRef: Any?,
            property: KProperty<*>,
            value: T
    ) = settingsProvider().set(
            key = key,
            value = valueToString(value)
    )

    protected abstract fun valueToString(value: T): String
    protected abstract fun stringToValue(string: String): T

}