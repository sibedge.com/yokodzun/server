package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.server.db.connectors.ParameterDbConnector
import com.sibedge.yokodzun.server.db.connectors.YokodzunDbConnector
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class YokodzunsManager {

    @Autowired
    private lateinit var yokodzunDb: YokodzunDbConnector

    fun getAll() = yokodzunDb.getAll()

    fun throwIfNotExists(id: String) = yokodzunDb.throwIfNotExists(id)

}