package com.sibedge.yokodzun.server.managers.notifications

import com.google.firebase.messaging.Message
import com.sibedge.yokodzun.common.data.notification.YToUserNotification


fun YToUserNotification.createMessage(
        pushToken: String
) = Message.builder()
        .putData(YToUserNotification.SERIALIZATION_KEY_TO_USER, toUser)
        .putData(YToUserNotification.SERIALIZATION_KEY_CLASS, notification.className)
        .putData(YToUserNotification.SERIALIZATION_KEY_CONTENT, notification.json)
        .setToken(pushToken)
        .build()!!