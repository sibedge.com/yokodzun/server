package com.sibedge.yokodzun.server.managers

import com.google.protobuf.Api
import com.sibedge.yokodzun.common.exception.ApiException
import com.sibedge.yokodzun.common.utils.AuthUtils
import com.sibedge.yokodzun.server.db.connectors.ParameterDbConnector
import com.sibedge.yokodzun.server.db.connectors.SettingDbConnector
import com.sibedge.yokodzun.server.managers.settings.SettingsManager
import com.sibedge.yokodzun.server.utils.Utils
import org.apache.commons.codec.digest.DigestUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.RequestParam
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.takeIfNotEmpty


@Component
class AdminAuthManager {

    companion object {

        private val DEFAULT_ADMIN_PASSWORD_HASH = getPasswordHash("password")

        private const val HASH_SALT = "FB134B05C4E0448D9AC1D8C9CBA10D6E"

        private fun getPasswordHash(password: String): String =
                DigestUtils.sha256Hex(password + HASH_SALT)

    }

    @Autowired
    private lateinit var settings: SettingsManager

    @Autowired
    private lateinit var clientAppInstanceManager: ClientAppInstanceManager

    fun changePassword(
            oldPassword: String,
            newPassword: String
    ) {
        validatePasswordOrThrow(oldPassword, false)
        settings.adminPasswordHash = getPasswordHash(newPassword)
    }

    fun login(
            password: String,
            appInstanceUUID: String
    ): String {
        validatePasswordOrThrow(password, true)
        var authToken = settings.adminAuthToken.takeIfNotEmpty()
        if (authToken == null) {
            authToken = Utils.genUUID()
            settings.adminAuthToken = authToken
        }
        clientAppInstanceManager.updateLogin(appInstanceUUID, AuthUtils.ADMIN_LOGIN)
        return authToken
    }

    fun validateAuthTokenOrThrow(
            authToken: String
    ) {
        authToken.ifEmpty { throw ApiException.AUTHENTICATION }
        (authToken != settings.adminAuthToken).ifTrue { throw ApiException.AUTHENTICATION }
    }


    private fun validatePasswordOrThrow(
            password: String,
            failIfDefaultPassword: Boolean
    ) {
        val passwordHash = getPasswordHash(password)

        val settingsPasswordHash = settings.adminPasswordHash.takeIfNotEmpty()
                ?: DEFAULT_ADMIN_PASSWORD_HASH

        (passwordHash != settingsPasswordHash).ifTrue {
            throw ApiException.AUTHENTICATION
        }

        if (failIfDefaultPassword && passwordHash == DEFAULT_ADMIN_PASSWORD_HASH) {
            throw ApiException.ADMIN_PASSWORD_NOT_CONFIGURED
        }
    }

}