package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.server.db.connectors.BattleRaterDbConnector
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component


@Component
class AdminRaterManager {

    @Autowired
    private lateinit var battleRaterDb: BattleRaterDbConnector

    @Autowired
    protected lateinit var emailManager: EmailManager

    @Autowired
    protected lateinit var battlesManager: AdminBattlesManager

    fun addRatersForBattle(
            battleId: String,
            count: Int
    ): List<String> {
        val result = (0 until count).map { battleRaterDb.addForBattle(battleId) }
        sendNewRatersEmailMessage(battleId, result)
        return result
    }

    private fun sendNewRatersEmailMessage(
            battleId: String,
            raters: List<String>
    ) {
        val battleTitle = battlesManager.getById(battleId).description.title
        val subject = "Добавлены зрители битвы $battleTitle"
        emailManager.send(
                subject = battleTitle,
                text = raters.joinToString(
                        separator = "\n",
                        prefix = "$subject:\n"
                )
        )
    }

    fun remove(raterCode: String) = battleRaterDb.remove(raterCode)

    fun getBattleRaters(battleId: String) = battleRaterDb.getBattleRaters(battleId)

}