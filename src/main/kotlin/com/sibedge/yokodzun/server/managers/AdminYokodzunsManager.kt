package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.common.exception.ApiException
import com.sibedge.yokodzun.server.db.connectors.YokodzunDbConnector
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class AdminYokodzunsManager {

    @Autowired
    private lateinit var yokodzunDb: YokodzunDbConnector

    @Autowired
    private lateinit var battlesManager: AdminBattlesManager

    fun add() = yokodzunDb.add()

    fun remove(id: String) {
        checkAllowRemoveYokodzun(id)
        yokodzunDb.remove(id)
    }

    private fun checkAllowRemoveYokodzun(
            id: String
    ) {
        val battlesUsesYokodzun = battlesManager
                .getAll()
                .filter { it.yokodzunsIds.any { it == id } }
                .map { it.description.title }
        if (battlesUsesYokodzun.isNotEmpty()) {
            throw ApiException.raw("Данная ёкодзуна участвует в следующих битвах: ${battlesUsesYokodzun.joinToString(", ")}")
        }
    }

    fun updateDescription(
            id: String,
            description: Description
    ) = yokodzunDb.updateDescription(
            id = id,
            description = description
    )

}