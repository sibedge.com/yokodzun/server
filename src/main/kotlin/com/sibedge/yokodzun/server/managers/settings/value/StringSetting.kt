package com.sibedge.yokodzun.server.managers.settings.value

import com.sibedge.yokodzun.server.managers.settings.value.utils.Setting
import com.sibedge.yokodzun.server.managers.settings.value.utils.Settings


class StringSetting(
        key: String,
        settingsProvider: () -> Settings,
        defaultValue: String = ""
) : Setting<String>(
        settingsProvider = settingsProvider,
        key = key,
        defaultValue = defaultValue
) {

    override fun valueToString(value: String) = value
    override fun stringToValue(string: String) = string

}