package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.common.exception.ApiException
import com.sibedge.yokodzun.server.db.connectors.ClientAppInstanceDbConnector
import com.sibedge.yokodzun.server.utils.log.HSessionLog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.ifNull
import ru.hnau.jutils.tryOrNull
import java.util.*


@Component
class ClientAppInstanceManager {

    companion object {

        fun validateUUIDOrThrow(
                uuid: String,
                sessionLogger: HSessionLog
        ) {
            tryOrNull { UUID.fromString(uuid) }.ifNull {
                sessionLogger
                        .getMethodLog(ClientAppInstanceManager::class.java, "validateUUIDOrThrow")
                        .w("Client app instance '$uuid' is incorrect")
                throw ApiException.raw("Некорректный идентификатор экземпляра приложения '$uuid'")
            }
        }

        private const val PUSH_TOKEN_MIN_LENGTH = 1
        private const val PUSH_TOKEN_MAX_LENGTH = 255

        fun validatePushTokenOrThrow(
                pushToken: String,
                sessionLogger: HSessionLog
        ) {
            val length = pushToken.length
            if (length < PUSH_TOKEN_MIN_LENGTH || length > PUSH_TOKEN_MAX_LENGTH) {
                sessionLogger
                        .getMethodLog(ClientAppInstanceManager::class.java, "validatePushTokenOrThrow")
                        .w("Push token '$pushToken' is incorrect")
                throw ApiException.raw("Некорректный push token '$pushToken'")
            }
        }

    }

    @Autowired
    private lateinit var dbConnector: ClientAppInstanceDbConnector


    fun updateLogin(clientAppInstanceUUID: String, login: String) =
            dbConnector.updateLogin(clientAppInstanceUUID, login)

    fun clearLogin(clientAppInstanceUUID: String) =
            updateLogin(clientAppInstanceUUID, "")

    fun updatePushToken(clientAppInstanceUUID: String, pushToken: String) =
            dbConnector.updatePushToken(clientAppInstanceUUID, pushToken)

    fun getPushTokensForLogin(login: String) =
            dbConnector.getPushTokensForLogin(login)


}