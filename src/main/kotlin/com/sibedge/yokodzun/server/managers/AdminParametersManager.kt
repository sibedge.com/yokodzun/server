package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.common.data.helpers.Description
import com.sibedge.yokodzun.common.exception.ApiException
import com.sibedge.yokodzun.server.db.connectors.ParameterDbConnector
import com.sibedge.yokodzun.server.db.connectors.utils.query.DbQueryBuilder
import com.sibedge.yokodzun.server.db.connectors.utils.update.set
import com.sibedge.yokodzun.server.db.entities.ParameterDB
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class AdminParametersManager {

    @Autowired
    private lateinit var parametersDb: ParameterDbConnector

    @Autowired
    private lateinit var battlesManager: AdminBattlesManager

    fun add() = parametersDb.add()

    fun remove(id: String) {
        checkAllowRemoveParameter(id)
        parametersDb.remove(id)
    }

    private fun checkAllowRemoveParameter(
            id: String
    ) {
        val battlesUsesParameter = battlesManager
                .getAll()
                .filter { it.parameters.any { it.parameterId == id } }
                .map { it.description.title }
        if (battlesUsesParameter.isNotEmpty()) {
            throw ApiException.raw("Следующие битвы используют данный параметр: ${battlesUsesParameter.joinToString(", ")}")
        }
    }

    fun updateDescription(
            id: String,
            description: Description
    ) = parametersDb.updateDescription(
            id = id,
            description = description
    )

}