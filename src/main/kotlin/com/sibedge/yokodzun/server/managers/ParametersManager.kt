package com.sibedge.yokodzun.server.managers

import com.sibedge.yokodzun.server.db.connectors.ParameterDbConnector
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class ParametersManager {

    @Autowired
    private lateinit var parametersDb: ParameterDbConnector

    fun getAll() = parametersDb.getAll()

    fun throwIfNotExists(id: String) = parametersDb.throwIfNotExists(id)

}